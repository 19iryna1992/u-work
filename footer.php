<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package u_work
 */

if ( ! defined( 'ABSPATH' ) ) exit;

?>

</div><!-- #content -->

<footer class="o-footer">
     <?php get_template_part('template-parts/organisms/footer/top-footer'); ?>
     <?php get_template_part('template-parts/organisms/footer/middle-footer'); ?>
     <?php get_template_part('template-parts/organisms/footer/bottom-footer'); ?>
</footer>


</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
