<?php
$icon = get_field('cta_icon');
$content = get_field('cta_title');
$link = get_field('cta_link');
?>

<section class="s-sta">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="c-cta">
                    <?php if ($content): ?>
                        <div class="c-cta__content">
                            <?php if ($icon): ?>
                                <div class="c-cta__icon">
                                    <?php echo wp_get_attachment_image($icon['ID'], 'full') ?>
                                </div>
                            <?php endif; ?>
                            <?php echo $content ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($link): ?>
                        <div class="c-cta__link">
                            <a class="c-primary-button c-primary-button--yellow c-primary-button--around"
                               href="<?php echo $link['url'] ?>"
                               target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
