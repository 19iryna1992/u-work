<?php

if (!defined('ABSPATH')) exit;

?>

<div class="o-footer__top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav id="footer-navigation" class="c-footer-navigation">
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'footer-menu',
                        'menu_id' => 'footer-menu',
                        'menu_class' => 'c-footer-navigation__menu'
                    ]);
                    ?>
                </nav>
            </div>
        </div>
    </div>
</div>