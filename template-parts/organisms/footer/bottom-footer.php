<?php

if (!defined('ABSPATH')) exit;

?>

<div class="o-footer__bottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9">
                <a href="#" class="o-footer__bottom-link"><?php _e('Terms and Conditions', 'u_work'); ?></a>
                <a href="#" class="o-footer__bottom-link"><?php _e('Privacy Policy', 'u_work'); ?></a>
            </div>
    </div>
</div>