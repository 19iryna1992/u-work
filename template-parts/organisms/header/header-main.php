<?php

if (!defined('ABSPATH')) exit;

$logo = get_field('header_logo', 'option');

?>
<div class="container">
    <div class="row">
        <?php if ($logo): ?>
            <div class="col-2">
                <div class="o-header__logo">
                    <?php echo wp_get_attachment_image($logo['ID'], 'full') ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-8">
            <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
        </div>
    </div>
</div>