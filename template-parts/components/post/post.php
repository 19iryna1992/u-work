<?php

?>

<div class="c-post__content">

    <div class="c-post__title">
        <?= get_the_title() ?>
    </div>
    <div class="c-post__date">
        <?= get_the_date() ?>
    </div>
    <div class="c-post__content c-wysiwyg">
        <?= get_the_content() ?>
    </div>
</div>
