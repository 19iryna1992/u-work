<?php

/**
 * Heading Component
 */

if (!defined('ABSPATH')) exit;

if(!get_sub_field('heading_visibility')) return;

$title = get_sub_field('heading_title');
$color_title = get_sub_field('heading_title_color');
$link = get_sub_field('heading_link');

?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="c-section-header__row"> 
                <?php if ($title) : ?>
                    <h3 class="c-intro__title u-orange u-line"><?php echo $title; ?></h3>
                <?php endif; ?>
                <?php if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <div class="c-section-header__link-wrap">
                        <a class="c-section-header__link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                            <?php echo esc_html($link_title); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
