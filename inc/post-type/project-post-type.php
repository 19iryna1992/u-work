<?php

if (!defined('ABSPATH')) exit;

add_action('init', function () {

    $labels = u_work_post_type_labels('Project', 'Projects');

    $args = array(
        'description' => __('Projects', 'u_work'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_icon'     => 'dashicons-portfolio',
        'menu_position' => 20,
        'rewrite' => ['slug' => 'project', 'with_front' => false],
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type'    => 'post',
    );

    register_post_type('project', $args);
});

add_action('init', function () {

    $tax =  'project_category';
    $type = ['project'];

    $labels = u_work_post_type_labels('Project Category', 'Project Categories');

    $args = [
        'description'         => 'Categories',
        'labels'              => $labels,
        'hierarchical'        => true,
        'show_ui'             => true,
        'show_admin_column'   => true,
        'show_in_quick_edit'  => true,
        'show_in_menu'        => true,
        'rewrite'             => ['slug' => 'project-category', 'with_front' => false],
    ];

    register_taxonomy($tax, $type, $args);
});
