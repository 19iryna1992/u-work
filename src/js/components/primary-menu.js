const hasSubMenuItems = document.querySelectorAll('.menu-item-has-children');

hasSubMenuItems.forEach(item => {
    const subMenuBtn = item.firstElementChild;
    subMenuBtn.classList.add('c-primary-menu__sub-menu-btn');
    subMenuBtn.addEventListener('click', () => {
        item.querySelector('.sub-menu').classList.toggle("show");
    });
})