<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package u_work
 */

if (!defined('ABSPATH')) exit;

get_header();

while (have_posts()) : the_post();
    $cat = get_the_category();

    ?>

    <main id="main" role="main" tabindex="-1">
        <article class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="c-post__cat">Aktualności</h3>
                </div>
            </div>
            <div class="c-post">
                <div class="row">
                    <div class="col-12">
                        <?php get_template_part('template-parts/components/post/post-hero'); ?>
                        <?php get_template_part('template-parts/components/post/post'); ?>
                    </div>
                </div>
            </div>
        </article>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>