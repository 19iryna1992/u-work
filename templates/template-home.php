<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">
    
    <?php get_template_part('template-parts/sections/cta'); ?>

</main>

<?php get_footer(); ?>